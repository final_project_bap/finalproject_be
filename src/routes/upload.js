const express = require('express');
const { upload,checkImageFile  } = require('../ultils/multer.js');
const uploadController = require('../Controllers/imageController.js');

const router = express.Router();

router.get('/',uploadController.getImagesByUserId)
router.post('/upload/', upload, uploadController.uploadImage);
router.delete('/remove/:public_id', uploadController.removeImages);

router.post('/share', uploadController.shareImage);
router.get('/getShareImages',uploadController.getSharedImages)
router.post('/download/',uploadController.downloadImage)


router.get('/favorite/',uploadController.getImageFavorite)
router.post('/favorite/',uploadController.postImageFavorite);
router.post('/remove/favorite',uploadController.removeImageFavorite)

module.exports = router;
