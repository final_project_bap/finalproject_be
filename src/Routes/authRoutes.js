const express = require('express');
const authController = require('../Controllers/authController');
const router = express.Router();



router.post('/signup', authController.signup);
router.post('/login', authController.login);
router.post('/logout', authController.logout);
router.post('/refreshToken', authController.refreshToken);


module.exports = router;