const userRoutes = require('./userRoutes')
const authRoutes = require('./authRoutes');
const uploadRouter = require("./upload");
const albumRoutes = require('./albumRoutes')
const verifyToken = require('../Middleware/auth')

function routes (app) {
    app.use('/auth',authRoutes);
    app.use('/user',userRoutes)
    app.use('/images', verifyToken, uploadRouter)
    app.use('/album', verifyToken, albumRoutes)

}


module.exports = routes;