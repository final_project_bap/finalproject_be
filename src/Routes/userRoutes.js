const express = require('express');
const UserController = require("../controllers/userController")
const userRoutes= express.Router();

const verifyToken = require('../Middleware/auth')


userRoutes.get('/current_user',verifyToken,UserController.currentUser);
userRoutes.post('/forgotPassword',UserController.sendPasswordResetEmail);
userRoutes.post('/resetPassword/:token',UserController.resetPassword);
userRoutes.get('/getUserById/:id', verifyToken,UserController.getUserById);
userRoutes.put('/updateUser/:id',verifyToken ,UserController.updateUserById);
module.exports = userRoutes;