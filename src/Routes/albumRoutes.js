const express = require('express');
const AlbumController = require('../Controllers/albumController')
const albumRoutes = express.Router();
const verifyToken = require("../Middleware/auth")
albumRoutes.post('/createAlbum',  AlbumController.createAlbum)
albumRoutes.post('/addImageIntoAlbum/:albumId',AlbumController.addImageIntoAlbum)
albumRoutes.get('/getAlbumsByUserId',  AlbumController.getAlbumsByUserId);
albumRoutes.get('/getImageFromAlbum/:albumId',  AlbumController.getImageFromAlbum);
albumRoutes.put('/updateNameAlbum/:albumId',  AlbumController.updateNameAlbum);
albumRoutes.put('/addImages/:albumId',  AlbumController.addImages);
albumRoutes.put('/removeImages/:albumId', AlbumController.removeImages);
albumRoutes.delete('/deleteAlbum/:albumId',  AlbumController.deleteAlbum);

albumRoutes.post('/shareAlbum/:albumId',  AlbumController.shareAlbum)
albumRoutes.get('/getShareAlbums',AlbumController.getShareAlbums)
module.exports = albumRoutes;