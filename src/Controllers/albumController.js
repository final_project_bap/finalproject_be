const Album = require('../Models/albumModels')
// const Image = require('../Models/imageModels')
const nodemailer = require('nodemailer');
const User = require('../Models/userModels')
const mongoose = require('mongoose');
const ShareAlbum = require ('../Models/shareAlbumModels');
class AlbumController {

  async getImageFromAlbum (req, res){
    try {
      const { albumId } = req.params;
      const album = await Album.findById(albumId).populate('images');

      if (!album) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found',
        });
      }

      const images = album.images.map(image => ({
        _id: image._id,
        image_name: image.image_name,
        imageURL: image.imageURL,
        user_id: image.user_id,
        createdAt: image.createdAt,
        updatedAt: image.updatedAt,
        // Bạn có thể bỏ qua các trường không cần thiết khác nếu muốn
      }));

      return res.status(200).json({
        code: 200,
        message: 'Success',
        data: 
          images,
      
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  };

  async getAlbumsByUserId(req, res) {
    try {
      const { user_id } = req;
      console.log(user_id);
      const albums = await Album.find({ user_id: user_id },{_id:1 , album_name: 1});
      return res.status(200).json({
        code: 200,
        message: 'success',
        data: 
          albums,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  };
  async createAlbum(req, res) {
    try {
      const { user_id } = req;
      const { album_name } = req.body;
      const album = await Album.create({
        album_name,
        user_id: user_id,
      });

      return res.status(201).json({
        code: 201,
        message: 'Create album successfully',
        data: album,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        status: 'error',
        message: 'Internal server error',
      });
    }
  }

  async addImageIntoAlbum(req, res) {
    try {
      const { albumId } = req.params; // Sửa lại tên biến album_id thành albumId
      const { images } = req.body;
      const album = await Album.findById(albumId);
      if (!album) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found',
        });
      }
      album.images.push(...images);
      await album.save();
      return res.status(200).json({
        code: 200,
        message: 'Add images into album successfully',
        data: album,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        status: 'error',
        message: 'Internal server error',
      });
    }
  };

  async updateNameAlbum(req, res) {
    try {
      const { user_id } = req;
      const albumId = req.params.albumId;
      const { album_name } = req.body;

      const updatedAlbum = await Album.findOneAndUpdate(
        { _id: albumId, user_id: user_id },
        { $set: { album_name: album_name } },
        { new: true } // This ensures that you get the updated document in the response
      );

      if (!updatedAlbum) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found for the given user and albumId',
        });
      }

      res.status(200).json({
        status: 'success',
        data: {
          album: updatedAlbum,
        },
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  }
  async removeImages(req, res) {
    try {
      const { user_id } = req;
      const albumId = req.params.albumId;
      const { removeImages } = req.body;

      const album = await Album.findOne({ _id: albumId, user_id: user_id });

      if (!album) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found for the given user and albumId',
        });
      }
      if (removeImages && removeImages.length > 0) {
        album.images = album.images.filter(imageId => !removeImages.includes(imageId.toString()));
      }
      const updatedAlbum = await album.save();
      return res.status(200).json({
        code: 200,
        message: 'Remove successfully',
        data: {
          album: updatedAlbum,
        },
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }

  }
  async addImages(req, res) {
    try {
      const { user_id } = req;
      const albumId = req.params.albumId;
      const { addImages } = req.body;

      const album = await Album.findOne({ _id: albumId, user_id: user_id });

      if (!album) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found for the given user and albumId',
        });
      }

      if (addImages && addImages.length > 0) {
        album.images = [...album.images, ...addImages];
      }

      const updatedAlbum = await album.save();

      return res.status(200).json({
        code: 200,
        message: 'add image successfully',
        data: {
          album: updatedAlbum,
        },
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }

  }

 
  async deleteAlbum(req, res) {
    try {
      const { user_id } = req;
      const albumId = req.params.albumId;
  
      // Find the album
      const album = await Album.findOne({ _id: albumId, user_id: user_id });
  
      if (!album) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found for the given user and albumId',
          data: {},
        });
      }
  
      // Find and delete all shared albums associated with the original album
      const sharedAlbums = await ShareAlbum.find({ sharedAlbum: albumId });
  
      for (const sharedAlbum of sharedAlbums) {
        await ShareAlbum.deleteOne({ _id: sharedAlbum._id });
      }
  
      // Delete the original album
      await Album.deleteOne({ _id: albumId, user_id: user_id });
  
      console.log(`Album ${albumId} deleted successfully, along with shared albums`);
  
      return res.status(200).json({
        code: 200,
        message: 'Album deleted successfully',
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  }
  


  async shareAlbum(req, res) {
    try {
      const { user_id } = req;
      const { albumId } = req.params;
      const { recipients } = req.body; // Change the name to recipients and accept an array of email addresses or user IDs
  
      const album = await Album.findOne({ _id: albumId, user_id: user_id });
  
      if (!album) {
        return res.status(404).json({
          code: 404,
          message: 'Album not found for the given user and albumId',
        });
      }

      const sharedAlbumPromises = recipients.map(async (recipient) => {
        let recipientUser;
  
        // Determine whether recipient is an email or user ID
        if (recipient.includes('@')) {
          recipientUser = await User.findOne({ email: recipient });
        } else {
          recipientUser = await User.findById(recipient);
        }

        // Handle the case where the user is not found
        if (!recipientUser) {
          return {
            code: 404,
            message: `User not found with ${recipient}`,
          };
        }

        // Check if the user is sharing with themselves
        if (recipientUser._id.toString() === user_id.toString()) {
          return {
            code: 400,
            message: 'Cannot share album with yourself',
          };
        }

        // Check if the album has already been shared with the specified user
        const existingShare = await ShareAlbum.findOne({
          sharedAlbum: albumId,
          sharedWith: recipientUser._id,
        });

        if (existingShare) {
          return {
            code: 400,
            message: `Album has already been shared with ${recipient}`,
          };
        }

        const sharedAlbum = new ShareAlbum({
          sharedBy: user_id,
          sharedWith: recipientUser._id,
          sharedAlbum: albumId,
          sharedAt: new Date(),
        });

        await sharedAlbum.save();

        return {
          code: 200,
          message: `Album shared successfully with ${recipient}`,
        };
      });

      const results = await Promise.all(sharedAlbumPromises);

      return res.status(200).json(results);
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  }



  async getShareAlbums(req, res) {
    try {
      const  user_id  = req.user_id;
      console.log(user_id);
      const sharedAlbums = await ShareAlbum.find({ sharedWith: user_id }).populate('sharedAlbum');
      if (!sharedAlbums) {
        return res.status(204).json({
          code: 204,
          message: 'Album not found',
        });
      }
      const Albums = sharedAlbums.map(sharedAlbum => sharedAlbum.sharedAlbum);
      return res.status(200).json({
        code: 200,
        message: 'Success',
        data: Albums,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  }



}




module.exports = new AlbumController();
