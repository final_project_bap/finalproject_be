const userModels = require('../Models/userModels');
const Jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
dotenv.config();

function generateResetToken(user) {
    return Jwt.sign(
        { userId: user._id, email: user.email },
        process.env.JWT_SECRET,
        { expiresIn: '15m' }
    );
}

function verifyResetToken(token) {
    try {
        return Jwt.verify(token, process.env.JWT_SECRET);
    } catch (error) {
        console.error('Error in verifyResetToken service:', error);
        return null;
    }
}

async function sendEmail(email, token) {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'tammaxdog@gmail.com',
            pass: 'ewgy kxye hnia kpwz'
        }
    });

    token = token.replaceAll('.', '*')

    const resetLink = `http://localhost:5173/resetPassword/${token}`;

    const mailOptions = {
        from: 'tammaxdog@.com',
        to: email,
        subject: 'Password Reset',
        text: `Click on the following link to reset your password: ${resetLink}`,
    };

    await transporter.sendMail(mailOptions);
}

class UserController {

    async getUserById(req, res) {

        try {
            const { user_id } = req;

            const user = await userModels.findById({ _id: user_id })
            if (!user) {

                return res.json({
                    code: 404,
                    message: "User not found"
                });
            }
            return res.json({
                code: 200,
                message: "Successfully",
                data: {
                    user
                }
            });
        } catch (error) {
            return res.status(500).json({
                code: 500,
                message: "Internal Server Error",
            });
        }

    }

    async updateUserById(req, res) {
        try {
            const { user_id } = req;
            const dataUpdate = req.body;
            const updateUser = await userModels.findByIdAndUpdate(
                user_id,
                dataUpdate,
                { new: true }
            );
            return res.json({
                code: 200,
                message: "User updated successfully",
                data: updateUser
            });
        } catch (error) {
            return res.status(500).json({
                code: 500,
                message: "Internal Server Error",
            });
        }
    }
    async currentUser(req, res) {
        try {
            const { user_id } = req;
            console.log(user_id);
            const user = await userModels.findOne({ _id: user_id }).select("-_id name");
            return res.json({
                code: 200,
                message: "User retrieved successfully",
                data: user
            });
        } catch (error) {
            return res.status(500).json({
                code: 500,
                message: "Internal Server Error",
            });
        }
    }

    async sendPasswordResetEmail(req, res) {
        try {
            const {email} = req.body;
            console.log("email", email)
            const user = await userModels.findOne({ email: email });
            console.log("user", user)

            if (!user) {
                return res.status(404).json({
                    code: 404,
                    message: 'User not found',
                });
            }
            const resetToken = generateResetToken(user);
            await sendEmail(user.email, resetToken);

            return res.json({
                code: 200,
                message: 'Password reset email sent successfully',
            });
        } catch (error) {
            return res.status(500).json({
                code: 500,
                message: 'Internal Server Error',
            });
        }
    }

    async resetPassword(req, res) {
        try {
            const { token } = req.params;
            const { newPassword } = req.body;
            const user = verifyResetToken(token);
            if (!user) {
                if (!user) {
                    return res.json({
                        code: 400,
                        message: 'Invalid or expired token',
                    });
                }
            }

            const hashedPassword = await bcrypt.hash(newPassword, 10);
            console.log("hashedPassword", hashedPassword);
            await userModels.updateOne(
                { _id: user.userId },
                { $set: { password: hashedPassword } }
            );
            return res.json({
                code: 200,
                message: 'Password reset successfully',
            });
        } catch (error) {
            return res.status(500).json({
                code: 500,
                message: 'Internal Server Error',
            });
        }
    }
}

module.exports = new UserController();
