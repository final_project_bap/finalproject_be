const multer = require('multer');

const cloudinary = require('cloudinary').v2;

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});
const allowedImageExtensions = ['.jpg', '.jpeg', '.png', '.gif'];
const storage = multer.memoryStorage();
const upload = multer({ storage: storage , limits: { fileSize: 1024 * 1024 * 5 }}).array('image', 10);


function checkImageFile(req, res, next) {
  if (!req.file) {
    return res.status(400).json({ code: 400, message: 'No file uploaded' });
  }
  if (req.file.size > 10 * 1024 * 1024) {
    return res.status(400).json({ code: 400, message: 'File size exceeds the limit' });
  }

  const fileExtension = req.file.originalname.toLowerCase().substring(req.file.originalname.lastIndexOf('.'));

  if (!allowedImageExtensions.includes(fileExtension)) {
    return res.status(400).json({ code: 400, message: 'File is not an image' });
  }

  // Nếu không có lỗi, cho phép tiếp tục xử lý yêu cầu
  next();
}

module.exports = { upload, cloudinary, checkImageFile };