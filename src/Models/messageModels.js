    const mongoose = require('mongoose');

    const messageSchema = new mongoose.Schema({
    chatID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Chat',
        required: [true, 'A message must have a chat'],
    },
    userID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'A message must have a user'],
    },
    content: {
        type: String,
        required: [true, 'A message must have content'],
    },
    sent_at: {
        type: Date,
        default: Date.now,
    },
    });

    const Message = mongoose.model('Message', messageSchema);

    module.exports = Message;
