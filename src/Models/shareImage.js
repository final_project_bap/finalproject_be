const mongoose = require('mongoose');

const shareImageSchema = new mongoose.Schema({
  sharedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  image_name: {
    type: String,
    required: [true, 'An image must have a name'],
    trim: true,
    maxlength: [255, 'Image name must have less or equal than 255 characters'],
  },
  imageURL:{
    type:String,
    required:true
  },
  expirationDate:{
    type: Date,
  },
  sharedAt: { type: Date, default: Date.now },
});

const ShareImage = mongoose.model('ShareImage', shareImageSchema);

module.exports = ShareImage;