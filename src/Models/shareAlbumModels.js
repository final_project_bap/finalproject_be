const mongoose = require('mongoose');

const shareAlbumSchema = new mongoose.Schema({
  sharedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  sharedWith: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  sharedAlbum: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Album',
    required: true,
  },
  sharedAt: { 
    type: Date, 
    default: Date.now 
  },
});

const ShareAlbum = mongoose.model('ShareAlbum', shareAlbumSchema);

module.exports = ShareAlbum;
