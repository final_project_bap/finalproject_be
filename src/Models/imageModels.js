const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
  image_name: {
    type: String,
    required: [true, 'An image must have a name'],
    trim: true,
    maxlength: [255, 'Image name must have less or equal than 255 characters'],
  },
  description:{
    type: String,
    required:false,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  public_id: {
    type: String,
    required: true
  },
  imageURL:{
    type:String,
    required:true
  },
  updated_at: {
    type: Date,
    default: Date.now,
  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  status: {
    type: Boolean,
    default: true,
  },
  favorite_image: {
    type: Boolean,
    default: false,
  },
});

const Image = mongoose.model('Image', imageSchema);

module.exports = Image;
