const mongoose = require('mongoose');

const chatSchema = new mongoose.Schema({
  userID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'A chat must have a user'],
  },
  photoID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Photo',
    required: [true, 'A chat must have a photo'],
  },
  started_at: {
    type: Date,
    default: Date.now,
  },
});

const Chat = mongoose.model('Chat', chatSchema);

module.exports = Chat;
