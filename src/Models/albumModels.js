const mongoose = require('mongoose');

const albumSchema = new mongoose.Schema({
  album_name: {
    type: String,
    required: [true, 'An album must have a name'],
    trim: true,
    maxlength: [255, 'Album name must have less or equal than 255 characters'],
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    default: Date.now,
  },
  images: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Image',
  }],
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Album = mongoose.model('Album', albumSchema);

module.exports = Album;
