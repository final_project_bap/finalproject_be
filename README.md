## Prerequisites
Before you begin, ensure you have the following installed on your machine:

Node.js: This project requires Node.js. Download and install it from [https://nodejs.org/]. The project was developed with Node.js version v21.3.0

MongoDB: Make sure you have MongoDB installed. You can download it from [https://www.mongodb.com/try/download/community]. The project was developed with MongoDB version 8.0.3
## Installation
**Clone the repository**
1. Clone the repo:
```https://gitlab.com/final_project_bap/finalproject_be.git```
```bash
cd finalproject_bap
```
## Install dependencies:
```bash
npm install
```
## Run the application:
```bash
npm start
```
